"use strict";
//                  FORM VALIDATION              //

//Allow Numbers Only
const inputDigitsOnly = document.getElementById('phone');
inputDigitsOnly.addEventListener('keypress', (evt) => {
    const allowedNum = evt.keyCode;
    if (!(allowedNum >= 48 && allowedNum <= 57)) evt.preventDefault();
});


// const error = document.querySelector('.error');
const form = document.getElementById('form');
form.addEventListener('submit', (evt) => {

    const inputs = document.querySelectorAll('input');
    inputs.forEach(elem => {
        if (elem.value == "") {
            evt.preventDefault();
            requiredFiled();
        }
    });

    //prevent if email is incorrect
    const email = document.getElementById('email');
    if (!email.validity.valid) evt.preventDefault();

    //prevent if phone not at 10 chars
    const phone = document.getElementById('phone');
    const phoneValue = phone.value;
    const phoneLength = phoneValue.length;
    if (phoneLength != 10) evt.preventDefault();


}, false);



//set Custom Errors
const errors = document.querySelectorAll('.error');
const requiredFiled = function () {
    errors.forEach(err => {
        err.innerHTML = "This field is required";
        err.className = "error active";
    });
};
//incase in the future want to rest...
// const requiredFiledCorrect = function () {
//     errors.forEach(unerr => {
//         unerr.innerHTML = "";
//         unerr.className = 'error';
//     })
// }
